const { FormateData } = require("../utils");
const { PrismaClient } = require("@prisma/client");
const { APIError } = require("../utils/app-errors");
const TableDynamic = require("../utils/table-formater");

class CheckupService {
  constructor() {
    this.prisma = new PrismaClient().checkup;
  }

  async DeleteCheckup() {
    try {
      const deleteCheckup = await this.prisma.deleteMany({});
      return FormateData({ deleteCheckup });
    } catch (error) {
      throw new APIError("delete checkup failed", 400, "delete checkup failed");
    }
  }

  async CreateCheckup(params) {
    const { diseaseCategoryId, ageRangeId, patientTypeId, gender, amount } =
      params;

    try {
      const find = await this.prisma.findFirst({
        where: {
          disease_category_id: diseaseCategoryId,
          age_range_id: ageRangeId,
          patient_type_id: patientTypeId,
          gender,
        },
      });

      if (find) {
        const checkup = await this.prisma.update({
          where: {
            id: find.id,
          },
          data: {
            amount: {
              increment: amount,
            },
          },
        });

        return FormateData({ checkup });
      }

      const checkup = await this.prisma.create({
        data: {
          age_range_id: ageRangeId,
          disease_category_id: diseaseCategoryId,
          patient_type_id: patientTypeId,
          gender,
          amount,
        },
      });

      return FormateData({ checkup });
    } catch (error) {
      throw new APIError("upsert checkup failed", 400, "upsert checkup failed");
    }
  }

  async ReadCheckup(params) {
    // const { code, page, limit } = params;
    // const skip = (parseInt(page) - 1) * parseInt(limit);
    // const take = parseInt(limit);

    try {
      const formatTable = await TableDynamic.getDataSource();
      return FormateData(formatTable);
    } catch (error) {
      console.log(error);
      throw new APIError("read checkup failed", 400, "read checkup failed");
    }
  }
}

module.exports = CheckupService;
