const { FormateData } = require("../utils");
const { PrismaClient } = require("@prisma/client");
const { APIError } = require("../utils/app-errors");

class DownloadService {
  constructor() {
    this.prisma = new PrismaClient().checkup;
  }

  async ReadCheckup() {
    try {
      const checkup = await this.prisma.findMany({
        include: {
          disease_category: { select: { name: true, code: true } },
          age_range: { select: { name: true } },
          patient_type: { select: { name: true } },
        },
        orderBy: [
          {
            disease_category: { code: "asc" },
          },
          { age_range: { name: "asc" } },
          { patient_type: { name: "asc" } },
          { gender: "asc" },
        ],
      });
      return FormateData({ checkup });
    } catch (error) {
      throw new APIError("read checkup failed", 400, "read checkup failed");
    }
  }
}

module.exports = DownloadService;
