const {
  FormateData,
  GeneratePassword,
  GenerateSalt,
  GenerateSignature,
  ValidatePassword,
  DecodeSignature,
} = require("../utils");
const { APIError } = require("../utils/app-errors");
const { PrismaClient } = require("@prisma/client");

class UserService {
  constructor() {
    this.prisma = new PrismaClient().user;
  }

  async UserInfo(params) {
    try {
      const user = await DecodeSignature(params);
      return FormateData({ user });
    } catch (error) {
      throw new APIError("User not found", 401, "user not found");
    }
  }

  async SignUp(params) {
    const { username, password } = params;

    try {
      const salt = await GenerateSalt();
      const userPassword = await GeneratePassword(password, salt);

      const createUser = await this.prisma.create({
        data: { username, password: userPassword, salt },
      });

      return FormateData({ id: createUser.id });
    } catch (error) {
      throw new APIError("Signup Failed", 401, "signup failed");
    }
  }

  async SignIn(params) {
    const { username, password } = params;

    try {
      const existingUser = await this.prisma.findUnique({
        where: { username },
      });

      if (existingUser) {
        const validPassword = await ValidatePassword(
          password,
          existingUser.password,
          existingUser.salt
        );

        if (validPassword) {
          const token = await GenerateSignature({
            username: existingUser.username,
            id: existingUser.id,
          });

          return FormateData({
            id: existingUser.id,
            token,
          });
        }
      }

      return FormateData(null);
    } catch (error) {
      throw new APIError("SignIn Failed", 401, "wrong username or password");
    }
  }
}

module.exports = UserService;
