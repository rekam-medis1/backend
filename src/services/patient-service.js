const { FormateData } = require("../utils");
const { PrismaClient } = require("@prisma/client");
const { APIError } = require("../utils/app-errors");

class PatientService {
  constructor() {
    this.prisma = new PrismaClient().patient;
  }

  async CreatePatient(params) {
    const { name } = params;

    try {
      const patient = await this.prisma.create({
        data: { name },
      });
      return FormateData({ patient });
    } catch (error) {
      throw new APIError("create patient failed", 400, "create patient failed");
    }
  }

  async ReadAllPatient(params) {
    const { name, page, limit } = params;
    const offset = (parseInt(page) - 1) * parseInt(limit);

    try {
      const patient = await this.prisma.findMany({
        where: { name: { contains: name } },
        take: limit && parseInt(limit),
        skip: page && offset,
      });
      return FormateData({ patient });
    } catch (error) {
      throw new APIError("read all patient failed", 400, "read patient failed");
    }
  }

  async ReadPatientById(patientId) {
    try {
      const patient = await this.prisma.findUnique({
        where: { id: patientId },
      });
      return FormateData({ patient });
    } catch (error) {
      throw new APIError("read patient failed", 400, "read patient failed");
    }
  }

  async UpdatePatient(params) {
    const { name, patientId } = params;

    try {
      const patient = await this.prisma.update({
        where: { id: patientId },
        data: { name },
      });
      return FormateData({ patient });
    } catch (error) {
      throw new APIError("update patient failed", 400, "update patient failed");
    }
  }

  async DeletePatient(patientId) {
    try {
      const patient = await this.prisma.delete({
        where: { id: patientId },
      });
      return FormateData({ patient });
    } catch (error) {
      throw new APIError("delete patient failed", 400, "dalete patient failed");
    }
  }
}

module.exports = PatientService;
