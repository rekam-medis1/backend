const { FormateData, NestedArrayFormater } = require("../utils");
const { PrismaClient } = require("@prisma/client");
const { APIError } = require("../utils/app-errors");

class DiseaseCategoryService {
  constructor() {
    this.prisma = new PrismaClient().disease_category;
  }

  async CreateDiseaseCategory(params) {
    const { name, code, parent } = params;

    try {
      const diseaseCategory = await this.prisma.create({
        data: {
          name,
          code,
          parent,
        },
      });
      return FormateData({ diseaseCategory });
    } catch (error) {
      throw new APIError(
        "create disease category failed",
        400,
        "create disease category failed"
      );
    }
  }

  async ReadAllDiseaseCategory() {
    try {
      const diseaseCategory = await this.prisma.findMany();
      return FormateData({ diseaseCategory });
    } catch (error) {
      throw new APIError(
        "read all disease category failed",
        400,
        "read disease category failed"
      );
    }
  }

  async ReadDiseaseCategoryById(diseaseCategoryId) {
    try {
      const diseaseCategory = await this.prisma.findUnique({
        where: { id: Number(diseaseCategoryId) },
      });
      return FormateData({ diseaseCategory });
    } catch (error) {
      throw new APIError(
        "read disease category failed",
        400,
        "read disease category failed"
      );
    }
  }

  async ReadDiseaseCategoryParent(params) {
    const { code } = params;
    try {
      const diseaseCategory = await this.prisma.findMany({
        where: { parent: null, code: { contains: code } },
        orderBy: { code: "asc" },
      });
      return FormateData({ diseaseCategory });
    } catch (error) {
      throw new APIError(
        "read disease category failed",
        400,
        "read disease category failed"
      );
    }
  }

  async ReadDiseaseCategoryChildren(payload) {
    const { code } = payload;
    try {
      const diseaseCategory = await this.prisma.findMany({
        where: { NOT: { parent: null }, code: { contains: code } },
        orderBy: { code: "asc" },
      });
      return FormateData({ diseaseCategory });
    } catch (error) {
      throw new APIError(
        "read disease category failed",
        400,
        "read disease category failed"
      );
    }
  }

  async ReadDiseaseCategoryNested(params) {
    const { name, limit, page } = params;
    const skip = (parseInt(page) - 1) * parseInt(limit);
    const take = parseInt(limit);

    try {
      const diseaseCategoryCount = await this.prisma.count();
      const diseaseCategory = await this.prisma.findMany({
        orderBy: { code: "asc" },
        where: { name: { contains: name } },
        skip,
        take,
      });

      const getParent = async (idParent) => {
        const parent = await this.prisma.findUnique({
          where: { id: idParent },
        });

        return parent;
      };

      const diseaseNested = await Promise.all(
        diseaseCategory.map(async (element) => {
          if (element.parent !== null) {
            const parent = await getParent(element.parent);
            return { ...element, parentName: parent.name };
          }

          return { ...element };
        })
      );

      return FormateData({
        diseaseNested,
        totalItems: !name ? diseaseCategoryCount : diseaseNested.length,
        currentPage: parseInt(page),
      });
    } catch (error) {
      throw new APIError(
        "read disease category failed",
        400,
        "read disease category failed"
      );
    }
  }

  async UpdateDiseaseCategory(params) {
    const { name, code, parent, diseaseCategoryId } = params;

    try {
      const diseaseCategory = await this.prisma.update({
        where: { id: Number(diseaseCategoryId) },
        data: {
          name,
          code,
          parent,
        },
      });
      return FormateData({ diseaseCategory });
    } catch (error) {
      throw new APIError(
        "update disease category failed",
        400,
        "update disease category failed"
      );
    }
  }

  async DeleteDiseaseCategory(diseaseCategoryId) {
    try {
      const diseases = await this.prisma.findMany({
        where: { parent: Number(diseaseCategoryId) },
      });
      console.log(diseases.length);
      if (diseases.length !== 0) throw new Error("cannot delete this data");
      const diseaseCategory = await this.prisma.delete({
        where: { id: Number(diseaseCategoryId) },
      });
      return FormateData({ diseaseCategory });
    } catch (error) {
      throw new APIError("delete disease category failed", 400, error.message);
    }
  }
}

module.exports = DiseaseCategoryService;
