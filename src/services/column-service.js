const { FormateData } = require("../utils");
const { PrismaClient } = require("@prisma/client");
const { APIError } = require("../utils/app-errors");
const { formatColumn } = require("../utils/format-columns");

class ColumnsService {
  constructor() {
    this.ageRange = new PrismaClient().age_range;
    this.patientType = new PrismaClient().patient_type;
  }

  async readColumn() {
    try {
      const ageRange = await this.ageRange.findMany({
        orderBy: { name: "asc" },
      });
      const patientType = await this.patientType.findMany();
      const columns = formatColumn({ ageRange, patientType });
      return FormateData({ columns });
    } catch (error) {
      throw new APIError("Read Column failed", 400, "read column failed");
    }
  }
}

module.exports = ColumnsService;
