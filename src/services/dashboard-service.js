const { FormateData } = require("../utils");
const { PrismaClient } = require("@prisma/client");
const { APIError } = require("../utils/app-errors");

class DashboardService {
  constructor() {
    this.prisma = new PrismaClient();
  }

  async ReadPatienAmount() {
    try {
      const amount = await this.prisma.checkup.aggregate({
        _sum: { amount: true },
      });
      return FormateData({ amount: amount._sum.amount });
    } catch (error) {
      throw new APIError(
        "create age range failed",
        400,
        "create age range failed"
      );
    }
  }
}

module.exports = DashboardService;
