const { FormateData } = require("../utils");
const { PrismaClient } = require("@prisma/client");
const { APIError } = require("../utils/app-errors");

class AgeRangeService {
  constructor() {
    this.prisma = new PrismaClient();
  }

  async CreateAgeRange(params) {
    const { name } = params;

    try {
      const ageRange = await this.prisma.age_range.create({
        data: { name },
      });
      return FormateData({ ageRange });
    } catch (error) {
      throw new APIError(
        "create age range failed",
        400,
        "create age range failed"
      );
    }
  }

  async ReadAllAgeRange(payload) {
    const { name } = payload;
    try {
      if (name) {
        const ageRange = await this.prisma.age_range.findMany({
          where: { name: { contains: name } },
        });

        return FormateData({ ageRange });
      }
      const ageRange = await this.prisma.age_range.findMany({
        where: { name: { contains: name } },
        orderBy: { createdAt: "asc" },
      });
      return FormateData({ ageRange });
    } catch (error) {
      throw new APIError(
        "read all age range failed",
        400,
        "read age range failed"
      );
    }
  }

  async UpdateAgeRange(params) {
    const { name, ageRangeId } = params;

    try {
      const ageRange = await this.prisma.age_range.update({
        where: { id: Number(ageRangeId) },
        data: { name },
      });
      return FormateData({ ageRange });
    } catch (error) {
      throw new APIError(
        "update age range failed",
        400,
        "update age range failed"
      );
    }
  }

  /**
   *
   * @param {number} ageRangeId
   * @returns
   */
  async DeleteAgeRange(ageRangeId) {
    try {
      const ageRange = await this.prisma.age_range.delete({
        where: { id: Number(ageRangeId) },
      });
      return FormateData({ ageRange });
    } catch (error) {
      throw new APIError(
        "delete age range failed",
        400,
        "delete age range failed"
      );
    }
  }
}

module.exports = AgeRangeService;
