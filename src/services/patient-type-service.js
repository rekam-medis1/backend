const { FormateData } = require("../utils");
const { PrismaClient } = require("@prisma/client");
const { APIError } = require("../utils/app-errors");

class PatientTypeService {
  constructor() {
    this.prisma = new PrismaClient().patient_type;
  }

  async CreatePatientType(params) {
    const { name } = params;

    try {
      const patientType = await this.prisma.create({
        data: { name },
      });
      return FormateData({ patientType });
    } catch (error) {
      throw new APIError(
        "create patient type failed",
        400,
        "create patient failed"
      );
    }
  }

  async ReadAllPatientType(query) {
    const { search } = query;
    try {
      const patientType = await this.prisma.findMany({
        where: { name: { contains: search } },
      });
      return FormateData({ patientType });
    } catch (error) {
      throw new APIError(
        "read all patient type failed",
        400,
        "read patient failed"
      );
    }
  }

  async UpdatePatientType(params) {
    const { name, patientTypeId } = params;

    try {
      const patientType = await this.prisma.update({
        where: { id: Number(patientTypeId) },
        data: { name },
      });
      return FormateData({ patientType });
    } catch (error) {
      throw new APIError(
        "update patient type failed",
        400,
        "update patient failed"
      );
    }
  }

  async DeletePatientType(patientTypeId) {
    try {
      const patientType = await this.prisma.delete({
        where: { id: Number(patientTypeId) },
      });
      return FormateData({ patientType });
    } catch (error) {
      throw new APIError(
        "delete patient type failed",
        400,
        "delete patient failed"
      );
    }
  }
}

module.exports = PatientTypeService;
