const { FormateData } = require("../utils");
const { PrismaClient } = require("@prisma/client");
const { APIError } = require("../utils/app-errors");

class PuskesmasService {
  constructor() {
    this.prisma = new PrismaClient().puskesmas;
  }

  async CreatePuskesmas(params) {
    const { name } = params;

    try {
      const puskesmas = await this.prisma.create({
        data: { name },
      });
      return FormateData({ puskesmas });
    } catch (error) {
      throw new APIError(
        "create puskesmas failed",
        400,
        "create puskesmas failed"
      );
    }
  }

  async ReadAllPuskesmas() {
    try {
      const puskesmas = await this.prisma.findMany();
      return FormateData({ puskesmas });
    } catch (error) {
      throw new APIError(
        "read all puskesmas failed",
        400,
        "read puskesmas failed"
      );
    }
  }

  async ReadPuskesmasById(puskesmasId) {
    try {
      const puskesmas = await this.prisma.findUnique({
        where: { id: puskesmasId },
      });
      return FormateData({ puskesmas });
    } catch (error) {
      throw new APIError("read puskesmas failed", 400, "read puskesmas failed");
    }
  }

  async UpdatePuskesmas(params) {
    const { name, puskesmasId } = params;

    try {
      const puskesmas = await this.prisma.update({
        where: { id: puskesmasId },
        data: { name },
      });
      return FormateData({ puskesmas });
    } catch (error) {
      throw new APIError(
        "update puskesmas failed",
        400,
        "update puskesmas failed"
      );
    }
  }

  async DeletePuskesmas(puskesmasId) {
    try {
      const puskesmas = await this.prisma.delete({
        where: { id: puskesmasId },
      });
      return FormateData({ puskesmas });
    } catch (error) {
      throw new APIError(
        "delete puskesmas failed",
        400,
        "delete puskesmas failed"
      );
    }
  }
}

module.exports = PuskesmasService;
