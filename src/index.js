const express = require("express");
const exportsApp = require("./express-app");
const { PORT } = require("./config");

const Server = async () => {
  const app = express();

  await exportsApp(app);

  app
    .listen(PORT, () => {
      console.log(`app running on port ${PORT}`);
    })
    .on("error", (error) => {
      console.log(error);
      process.exit;
    });
};

Server();
