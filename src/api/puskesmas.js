const PuskesmasService = require("../services/puskesmas-service");
const UserAuth = require("./middlewares/auth");

module.exports = (app) => {
  const service = new PuskesmasService();

  app.post("/master/puskesmas", UserAuth, async (req, res, next) => {
    try {
      const { name } = req.body;
      const { data } = await service.CreatePuskesmas({ name });
      res.json({ data });
    } catch (error) {
      next(error);
    }
  });

  app.put(
    "/master/puskesmas/:puskesmasId",
    UserAuth,
    async (req, res, next) => {
      try {
        const { name } = req.body;
        const { puskesmasId } = req.params;
        const { data } = await service.UpdatePuskesmas({
          name,
          puskesmasId,
        });
        res.json({ data });
      } catch (error) {
        next(error);
      }
    }
  );

  app.get("/master/puskesmas", UserAuth, async (req, res, next) => {
    try {
      const { data } = await service.ReadAllPuskesmas();
      res.json({ data });
    } catch (error) {
      next(error);
    }
  });

  app.get(
    "/master/puskesmas/:puskesmasId",
    UserAuth,
    async (req, res, next) => {
      try {
        const { puskesmasId } = req.params;
        const { data } = await service.ReadPuskesmasById(puskesmasId);
        res.json({ data });
      } catch (error) {
        next(error);
      }
    }
  );

  app.delete(
    "/master/puskesmas/:puskesmasId",
    UserAuth,
    async (req, res, next) => {
      try {
        const { puskesmasId } = req.params;
        const { data } = await service.DeletePuskesmas(puskesmasId);
        res.json({ data });
      } catch (error) {
        next(error);
      }
    }
  );
};
