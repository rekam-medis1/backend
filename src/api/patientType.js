const PatientTypeService = require("../services/patient-type-service");
const UserAuth = require("./middlewares/auth");

module.exports = (app) => {
  const service = new PatientTypeService();

  app.post("/master/patientType", UserAuth, async (req, res, next) => {
    try {
      const { name } = req.body;
      const { data } = await service.CreatePatientType({ name });
      res.json({ data });
    } catch (error) {
      next(error);
    }
  });

  app.put(
    "/master/patientType/:patientTypeId",
    UserAuth,
    async (req, res, next) => {
      try {
        const { name } = req.body;
        const { patientTypeId } = req.params;
        const { data } = await service.UpdatePatientType({
          name,
          patientTypeId,
        });
        res.json({ data });
      } catch (error) {
        next(error);
      }
    }
  );

  app.get("/master/patientType", UserAuth, async (req, res, next) => {
    const query = req.query;
    try {
      const { data } = await service.ReadAllPatientType(query);
      res.json({ data });
    } catch (error) {
      next(error);
    }
  });

  app.delete(
    "/master/patientType/:patientTypeId",
    UserAuth,
    async (req, res, next) => {
      try {
        const { patientTypeId } = req.params;
        const { data } = await service.DeletePatientType(patientTypeId);
        res.json({ data });
      } catch (error) {
        next(error);
      }
    }
  );
};
