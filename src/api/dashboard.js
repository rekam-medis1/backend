const DashboardService = require("../services/dashboard-service");
const UserAuth = require("./middlewares/auth");

module.exports = (app) => {
  const service = new DashboardService();

  app.get("/dashboard", UserAuth, async (req, res, next) => {
    try {
      const { data } = await service.ReadPatienAmount();
      res.json({ data });
    } catch (error) {
      next(error);
    }
  });
};
