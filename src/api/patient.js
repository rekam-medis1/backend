const PatientService = require("../services/patient-service");
const UserAuth = require("./middlewares/auth");

module.exports = (app) => {
  const service = new PatientService();

  app.post("/master/patient", UserAuth, async (req, res, next) => {
    try {
      const { name } = req.body;
      const { data } = await service.CreatePatient({ name });
      res.json({ data });
    } catch (error) {
      next(error);
    }
  });

  app.put("/master/patient/:patientId", UserAuth, async (req, res, next) => {
    try {
      const { name } = req.body;
      const { patientId } = req.params;
      const { data } = await service.UpdatePatient({ name, patientId });
      res.json({ data });
    } catch (error) {
      next(error);
    }
  });

  app.get("/master/patient", UserAuth, async (req, res, next) => {
    try {
      const { name, page, limit } = req.query;
      const { data } = await service.ReadAllPatient({ name, page, limit });
      res.json({ data });
    } catch (error) {
      next(error);
    }
  });

  app.get("/master/patient/:patientId", UserAuth, async (req, res, next) => {
    try {
      const { patientId } = req.params;
      const { data } = await service.ReadPatientById(patientId);
      res.json({ data });
    } catch (error) {
      next(error);
    }
  });

  app.delete("/master/patient/:patientId", UserAuth, async (req, res, next) => {
    try {
      const { patientId } = req.params;
      const { data } = await service.DeletePatient(patientId);
      res.json({ data });
    } catch (error) {
      next(error);
    }
  });
};
