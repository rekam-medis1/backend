const AgeRangeService = require("../services/age-range-service");
const UserAuth = require("./middlewares/auth");

module.exports = (app) => {
  const service = new AgeRangeService();

  app.post("/master/ageRange", UserAuth, async (req, res, next) => {
    try {
      const { name } = req.body;
      const { data } = await service.CreateAgeRange({ name });
      res.json({ data });
    } catch (error) {
      next(error);
    }
  });

  app.put("/master/ageRange/:ageRangeId", UserAuth, async (req, res, next) => {
    try {
      const { name } = req.body;
      const { ageRangeId } = req.params;
      const { data } = await service.UpdateAgeRange({ name, ageRangeId });
      res.json({ data });
    } catch (error) {
      next(error);
    }
  });

  app.get("/master/ageRange", UserAuth, async (req, res, next) => {
    const query = req.query;
    try {
      const { data } = await service.ReadAllAgeRange(query);
      res.json({ data });
    } catch (error) {
      next(error);
    }
  });

  app.delete(
    "/master/ageRange/:ageRangeId",
    UserAuth,
    async (req, res, next) => {
      try {
        const { ageRangeId } = req.params;
        const { data } = await service.DeleteAgeRange(ageRangeId);
        res.json({ data });
      } catch (error) {
        next(error);
      }
    }
  );
};
