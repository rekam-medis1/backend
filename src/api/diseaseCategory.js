const DiseaseCategoryService = require("../services/disease-category-service");
const UserAuth = require("./middlewares/auth");

module.exports = (app) => {
  const service = new DiseaseCategoryService();

  app.post("/master/diseaseCategory", UserAuth, async (req, res, next) => {
    try {
      const { name, code, parent } = req.body;
      const { data } = await service.CreateDiseaseCategory({
        name,
        code,
        parent,
      });
      res.json({ data });
    } catch (error) {
      next(error);
    }
  });

  app.put(
    "/master/diseaseCategory/:diseaseCategoryId",
    UserAuth,
    async (req, res, next) => {
      try {
        const { name, code, parent } = req.body;
        const { diseaseCategoryId } = req.params;
        const { data } = await service.UpdateDiseaseCategory({
          name,
          code,
          parent,
          diseaseCategoryId,
        });
        res.json({ data });
      } catch (error) {
        next(error);
      }
    }
  );

  app.get("/master/diseaseCategory", UserAuth, async (req, res, next) => {
    try {
      const { data } = await service.ReadAllDiseaseCategory();
      res.json({ data });
    } catch (error) {
      next(error);
    }
  });

  app.get(
    "/master/diseaseCategory/parent",
    UserAuth,
    async (req, res, next) => {
      const { code } = req.query;
      try {
        const { data } = await service.ReadDiseaseCategoryParent({ code });
        res.json({ data });
      } catch (error) {
        next(error);
      }
    }
  );

  app.get(
    "/master/diseaseCategory/children",
    UserAuth,
    async (req, res, next) => {
      const query = req.query;
      try {
        const { data } = await service.ReadDiseaseCategoryChildren(query);
        res.json({ data });
      } catch (error) {
        next(error);
      }
    }
  );

  app.get(
    "/master/diseaseCategory/nested",
    UserAuth,
    async (req, res, next) => {
      const { name, limit, page } = req.query;
      try {
        const { data } = await service.ReadDiseaseCategoryNested({
          name,
          limit,
          page,
        });
        res.json({ data });
      } catch (error) {
        next(error);
      }
    }
  );

  app.get(
    "/master/diseaseCategory/:diseaseCategoryId",
    UserAuth,
    async (req, res, next) => {
      try {
        const { diseaseCategoryId } = req.params;
        const { data } = await service.ReadDiseaseCategoryById(
          diseaseCategoryId
        );
        res.json({ data });
      } catch (error) {
        next(error);
      }
    }
  );

  app.delete(
    "/master/diseaseCategory/:diseaseCategoryId",
    UserAuth,
    async (req, res, next) => {
      try {
        const { diseaseCategoryId } = req.params;
        const { data } = await service.DeleteDiseaseCategory(diseaseCategoryId);
        res.json({ data });
      } catch (error) {
        next(error);
      }
    }
  );
};
