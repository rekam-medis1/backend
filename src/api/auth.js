const UserService = require("../services/auth-service");

module.exports = (app) => {
  const service = new UserService();

  app.post("/auth/signup", async (req, res, next) => {
    try {
      const { username, password } = req.body;
      const { data } = await service.SignUp({ username, password });
      return res.json({ data });
    } catch (error) {
      next(error);
    }
  });

  app.post("/auth/signin", async (req, res, next) => {
    try {
      const { username, password } = req.body;
      const { data } = await service.SignIn({ username, password });
      return res.json({ data });
    } catch (error) {
      next(error);
    }
  });

  app.post("/auth", async (req, res, next) => {
    try {
      const signature = req.headers.authorization;
      const data = await service.UserInfo(signature.split(" ")[1]);
      return res.json({ data });
    } catch (error) {
      next(error);
    }
  });
};
