const ColumnsService = require("../services/column-service");
const UserAuth = require("./middlewares/auth");

module.exports = (app) => {
  const service = new ColumnsService();

  app.get("/column", UserAuth, async (req, res, next) => {
    try {
      const { data } = await service.readColumn();
      res.json({ data });
    } catch (error) {
      next(error);
    }
  });
};
