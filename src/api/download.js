const DownloadService = require("../services/download-exel");
var XLSX = require("xlsx");
const UserAuth = require("./middlewares/auth");
const { Readable } = require("stream");

module.exports = (app) => {
  const service = new DownloadService();

  app.get("/download", async (req, res, next) => {
    try {
      const { data } = await service.ReadCheckup();

      const dataTable = data.checkup.map((element) => {
        return {
          "Code Icdx": element.disease_category.code,
          Diagnosa: element.disease_category.name,
          "Range Usia": element.age_range.name,
          "Tipe Pasien": element.patient_type.name,
          "Jenis Kelamin": element.gender,
          Jumlah: element.amount,
        };
      });

      const workbook = XLSX.utils.book_new();
      const worksheet = XLSX.utils.json_to_sheet(dataTable);
      worksheet["!cols"] = [{ wch: 20 }, { wch: 50 }, { wch: 30 }];

      XLSX.utils.book_append_sheet(workbook, worksheet, "Sheet1");
      //   XLSX.writeFile(workbook, "data.xlsx");
      const buffer = XLSX.write(workbook, {
        type: "buffer",
        bookType: "xlsx",
        compression: true,
      });
      res.writeHead(200, {
        "Content-Type":
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        "Content-Disposition": "attachment; filename=Rekap Rekam Medis.xlsx",
      });

      const stream = new Readable();
      stream.push(buffer);
      stream.push(null);

      stream.pipe(res);
    } catch (error) {
      next(error);
    }
  });
};
