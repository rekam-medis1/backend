const CheckupService = require("../services/checkup-service");
const UserAuth = require("./middlewares/auth");

module.exports = (app) => {
  const service = new CheckupService();

  app.delete("/checkup", UserAuth, async (req, res, next) => {
    try {
      const { data } = await service.DeleteCheckup();
      res.json({ data });
    } catch (error) {
      next(error);
    }
  });

  app.post("/checkup", UserAuth, async (req, res, next) => {
    try {
      const { diseaseCategoryId, ageRangeId, patientTypeId, gender, amount } =
        req.body;
      const { data } = await service.CreateCheckup({
        diseaseCategoryId,
        ageRangeId,
        patientTypeId,
        gender,
        amount,
      });
      res.json({ data });
    } catch (error) {
      next(error);
    }
  });

  app.get("/checkup", UserAuth, async (req, res, next) => {
    const query = req.query;
    try {
      const { data } = await service.ReadCheckup(query);
      res.json({ data });
    } catch (error) {
      next(error);
    }
  });
};
