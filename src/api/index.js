module.exports = {
  auth: require("./auth"),
  ageRange: require("./ageRange"),
  patientType: require("./patientType"),
  puskesmas: require("./puskesmas"),
  diseaseCategory: require("./diseaseCategory"),
  patient: require("./patient"),
  checkup: require("./checkup"),
  column: require("./column"),
  dashboard: require("./dashboard"),
  download: require("./download"),
};
