const express = require("express");
const cors = require("cors");
const HandleErrors = require("./utils/error-handler");
const {
  auth,
  ageRange,
  patientType,
  puskesmas,
  diseaseCategory,
  patient,
  checkup,
  column,
  dashboard,
  download,
} = require("./api");

module.exports = async (app) => {
  app.use(cors({ origin: true, credentials: true }));
  app.use(express.json({ limit: "10mb" }));

  // api

  app.use("/test", (req, res) => {
    res.json({ message: "mantap" });
  });

  auth(app);
  ageRange(app);
  patientType(app);
  puskesmas(app);
  diseaseCategory(app);
  patient(app);
  checkup(app);
  column(app);
  dashboard(app);
  download(app);

  // error handling
  app.use(HandleErrors);
};
