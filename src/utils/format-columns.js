const formatColumn = (params) => {
  const { ageRange = [], patientType = [] } = params;
  let newColumn = [];

  ageRange.sort((a, b) =>
    parseInt(a.name.split(" ")[0] - parseInt(b.name.split(" ")[0]))
  );

  newColumn = ageRange.map((element, index) => {
    return {
      title: element.name,
      children: [
        patientType.map((elementChild, indexChild) => {
          return {
            title: elementChild.name,
            children: [
              { title: "L", dataIndex: `${index}${indexChild}L` },
              { title: "P", dataIndex: `${index}${indexChild}P` },
            ],
          };
        }),
      ],
    };
  });

  return newColumn;
};

module.exports = { formatColumn };
