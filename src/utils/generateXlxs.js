const xlsx = require("json-as-xlsx");

const generateXlsx = (content) => {
  const data = [
    {
      sheet: "LB1",
      columns: [
        { value: (row) => row.disease_category.code, label: "Kode ICDX" },
        { value: (row) => row.disease_category.name, label: "Diagnosa" },
        { value: (row) => row.age_range.name, label: "Range Usia" },
        { value: (row) => row.patient_type.name, label: "Tipe" },
        { value: "gender", label: "Jenis Kelamin" },
        { value: "amount", label: "Jumlah" },
      ],

      content,
    },
  ];

  let settings = {
    fileName: "MySpreadsheet",
    extraLength: 3,
    writeMode: "writeFile",
    writeOptions: {},
    RTL: true,
  };

  xlsx(data, settings);
};

module.exports = { generateXlsx };
