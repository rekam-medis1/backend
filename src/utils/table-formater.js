const fs = require("fs");
// const prisma = require("./testing");
const { PrismaClient, Prisma } = require("@prisma/client");

const prisma = new PrismaClient();

class TableDynamic {
  columnsProperties() {
    return {
      includeColumnsHeader: {
        startOf: [
          {
            title: "Kode DX",
            dataIndex: "kodeParent",
            key: "kodeParent",
            width: 100,
            fixed: "left",
          },
          {
            title: "Kode ICDX",
            dataIndex: "code",
            key: "code",
            width: 100,
            fixed: "left",
          },
          {
            title: "Nama Penyakit",
            dataIndex: "name",
            key: "name",
            width: 200,
            fixed: "left",
          },
        ],
        lastOf: [
          {
            title: "Total",
            require: true,
            merge: function () {
              this.children.unshift(...this.temp);
              this.temp = [];
            },
            add: function ({ title }) {
              this.temp.push({
                title,
                children: [
                  {
                    title: "P",
                    dataIndex: `total_${title}_P`,
                    key: `total_${title}_P`,
                    width: 50,
                  },
                  {
                    title: "L",
                    dataIndex: `total_${title}_L`,
                    key: `total_${title}_L`,
                    width: 50,
                  },
                ],
              });
            },
            temp: [],
            children: [
              {
                title: "JKK",
                children: [
                  {
                    title: "P",
                    dataIndex: `total_sum_P`,
                    key: `total_sum_P`,
                    width: 50,
                  },
                  {
                    title: "L",
                    dataIndex: `total_sum_L`,
                    key: `total_sum_L`,
                    width: 50,
                  },
                ],
              },
            ],
          },
        ],
      },
      schema: function ({ title, key }) {
        return {
          title,
          dataIndex: key,
          key,
          width: 50,
        };
      },
    };
  }

  makeItemColumns(params) {
    const diseaseCategory = new Map();
    //adding default sum
    const patientType = [...params.patientType, { name: "JKK", id: null }];
    //static enum gender
    const genders = ["P", "L"];

    var index = 0;
    let sqlQueries = [];
    const columns = this.columnsProperties();
    var headerColumns = [...columns.includeColumnsHeader.startOf];

    var keyColumns = {};

    while (index < params.diseaseCategory.length) {
      let payload = { ...params.diseaseCategory[index], kodeParent: null };

      if (!Object.keys(keyColumns).length) {
        const columnTotals = columns.includeColumnsHeader.lastOf.find(
          (it) => it?.require
        );
        params.ageRanges.forEach((range) => {
          var typeChildren = [];
          patientType.forEach((tipe) => {
            if (tipe.id && sqlQueries.length < params.patientType.length) {
              sqlQueries.push([
                `SUM(IF(checkup.patient_type_id = ${tipe.id} && checkup.gender = 'L', checkup.amount, null)) AS total_${tipe.name}_L`,
                `SUM(IF(checkup.patient_type_id = ${tipe.id} && checkup.gender = 'P', checkup.amount, null)) AS total_${tipe.name}_P`,
              ]);
              columnTotals.add({ title: tipe.name });
            }
            var genderChildren = [];
            genders.forEach((gender) => {
              let key = `${range.id}_sum_${gender}`;
              if (tipe.id) {
                key = `${range.id}_${tipe.id}_${gender}`;
              }
              keyColumns[key] = null;
              genderChildren.push(columns.schema({ title: gender, key }));
            });
            typeChildren.push({ title: tipe.name, children: genderChildren });
          });
          headerColumns.push({
            title: range.name,
            children: typeChildren,
          });
          columnTotals.merge();
        });
      }

      if (!payload.parent) {
        payload.kodeParent = payload.code;
        payload.code = null;
      }

      diseaseCategory.set(payload.id, { ...payload, ...keyColumns });
      index++;
    }
    return {
      columns: [...headerColumns, ...columns.includeColumnsHeader.lastOf],
      diseaseCategory,
      sqlQueries: sqlQueries.flat().join(", "),
    };
  }

  static async getTotalsData(sqlQueries, data) {
    try {
      const includeSelect =
        "SUM(IF(checkup.gender = 'L', checkup.amount, null)) AS total_sum_L, SUM(IF(checkup.gender = 'P', checkup.amount, null)) AS total_sum_P";
      const sqlText = `SELECT disease_category.*, ${sqlQueries}, ${includeSelect} FROM disease_category LEFT JOIN checkup ON disease_category.id = checkup.disease_category_id GROUP BY disease_category.id;`;
      const items = await prisma.$queryRawUnsafe(sqlText);
      var index = 0;
      while (index < items.length) {
        const { id, name, code, parent, ...payload } = items[index];
        data.set(id, { ...data.get(id), ...payload });
        index++;
      }
      return data;
    } catch (error) {
      throw error;
    }
  }

  /**
   *
   * @param {{search?: string}} params
   */
  async getDataSource(params) {
    try {
      const [ageRanges, patientType, diseaseCategory, checkupList] =
        await Promise.all([
          prisma.$queryRaw`SELECT * FROM age_range ORDER BY createdAt;`,
          prisma.$queryRaw`SELECT * FROM patient_type;`,
          prisma.$queryRaw`SELECT * FROM disease_category;`,
          prisma.$queryRaw`SELECT * FROM checkup;`,
        ]);

      const mockColumns = this.makeItemColumns({
        ageRanges,
        patientType,
        diseaseCategory,
      });

      const data = await TableDynamic.getTotalsData(
        mockColumns.sqlQueries,
        mockColumns.diseaseCategory
      );

      var index = 0;
      while (index < checkupList.length) {
        const payload = checkupList[index];
        let item = data.get(payload.disease_category_id);
        const key = `${payload.age_range_id}_${payload.patient_type_id}_${payload.gender}`;
        if (key in item) {
          item[key] += payload.amount;
          item[`${payload.age_range_id}_sum_${payload.gender}`] +=
            payload.amount;
        }
        data.set(item.id, item);
        index++;
      }
      return {
        columns: mockColumns.columns,
        datasource: Array.from(data.values()),
      };
    } catch (error) {
      throw error;
    }
  }

  async exportJson(filename, data) {
    return new Promise((resolve, reject) => {
      fs.writeFile(filename, JSON.stringify(data, null, 2), (error) => {
        if (error) return reject(error);
        return resolve(`${filename} success created!!`);
      });
    });
  }
}

module.exports = new TableDynamic();

//   select
//  *
// from
//  disease_category k
// left join
//  checkup c
// on
//  k.id = c.id
