/*
  Warnings:

  - You are about to drop the column `rangeName` on the `age_range` table. All the data in the column will be lost.
  - Added the required column `name` to the `age_range` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `age_range` DROP COLUMN `rangeName`,
    ADD COLUMN `name` VARCHAR(191) NOT NULL;
