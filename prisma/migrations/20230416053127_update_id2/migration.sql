/*
  Warnings:

  - You are about to alter the column `parent` on the `disease_category` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `Int`.

*/
-- AlterTable
ALTER TABLE `disease_category` MODIFY `parent` INTEGER NULL;
