/*
  Warnings:

  - The primary key for the `age_range` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to alter the column `id` on the `age_range` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `Int`.
  - You are about to alter the column `age_range_id` on the `checkup` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `Int`.
  - You are about to alter the column `disease_category_id` on the `checkup` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `Int`.
  - You are about to alter the column `patient_type_id` on the `checkup` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `Int`.
  - The primary key for the `disease_category` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to alter the column `id` on the `disease_category` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `Int`.
  - You are about to alter the column `patientId` on the `log_checkup` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `Int`.
  - You are about to alter the column `puskesmasId` on the `log_checkup` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `Int`.
  - The primary key for the `patient` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to alter the column `id` on the `patient` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `Int`.
  - The primary key for the `patient_type` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to alter the column `id` on the `patient_type` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `Int`.
  - The primary key for the `puskesmas` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to alter the column `id` on the `puskesmas` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `Int`.

*/
-- DropForeignKey
ALTER TABLE `checkup` DROP FOREIGN KEY `checkup_age_range_id_fkey`;

-- DropForeignKey
ALTER TABLE `checkup` DROP FOREIGN KEY `checkup_disease_category_id_fkey`;

-- DropForeignKey
ALTER TABLE `checkup` DROP FOREIGN KEY `checkup_patient_type_id_fkey`;

-- DropForeignKey
ALTER TABLE `log_checkup` DROP FOREIGN KEY `log_checkup_patientId_fkey`;

-- DropForeignKey
ALTER TABLE `log_checkup` DROP FOREIGN KEY `log_checkup_puskesmasId_fkey`;

-- AlterTable
ALTER TABLE `age_range` DROP PRIMARY KEY,
    MODIFY `id` INTEGER NOT NULL AUTO_INCREMENT,
    ADD PRIMARY KEY (`id`);

-- AlterTable
ALTER TABLE `checkup` MODIFY `age_range_id` INTEGER NOT NULL,
    MODIFY `disease_category_id` INTEGER NOT NULL,
    MODIFY `patient_type_id` INTEGER NOT NULL;

-- AlterTable
ALTER TABLE `disease_category` DROP PRIMARY KEY,
    MODIFY `id` INTEGER NOT NULL AUTO_INCREMENT,
    ADD PRIMARY KEY (`id`);

-- AlterTable
ALTER TABLE `log_checkup` MODIFY `patientId` INTEGER NOT NULL,
    MODIFY `puskesmasId` INTEGER NOT NULL;

-- AlterTable
ALTER TABLE `patient` DROP PRIMARY KEY,
    MODIFY `id` INTEGER NOT NULL AUTO_INCREMENT,
    ADD PRIMARY KEY (`id`);

-- AlterTable
ALTER TABLE `patient_type` DROP PRIMARY KEY,
    MODIFY `id` INTEGER NOT NULL AUTO_INCREMENT,
    ADD PRIMARY KEY (`id`);

-- AlterTable
ALTER TABLE `puskesmas` DROP PRIMARY KEY,
    MODIFY `id` INTEGER NOT NULL AUTO_INCREMENT,
    ADD PRIMARY KEY (`id`);

-- AddForeignKey
ALTER TABLE `checkup` ADD CONSTRAINT `checkup_disease_category_id_fkey` FOREIGN KEY (`disease_category_id`) REFERENCES `disease_category`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `checkup` ADD CONSTRAINT `checkup_age_range_id_fkey` FOREIGN KEY (`age_range_id`) REFERENCES `age_range`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `checkup` ADD CONSTRAINT `checkup_patient_type_id_fkey` FOREIGN KEY (`patient_type_id`) REFERENCES `patient_type`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `log_checkup` ADD CONSTRAINT `log_checkup_patientId_fkey` FOREIGN KEY (`patientId`) REFERENCES `patient`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `log_checkup` ADD CONSTRAINT `log_checkup_puskesmasId_fkey` FOREIGN KEY (`puskesmasId`) REFERENCES `puskesmas`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
