/*
  Warnings:

  - A unique constraint covering the columns `[age_rangeId]` on the table `checkup` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[patient_typeId]` on the table `checkup` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX `checkup_age_rangeId_key` ON `checkup`(`age_rangeId`);

-- CreateIndex
CREATE UNIQUE INDEX `checkup_patient_typeId_key` ON `checkup`(`patient_typeId`);
