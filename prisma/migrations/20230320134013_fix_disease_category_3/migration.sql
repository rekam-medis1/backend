/*
  Warnings:

  - A unique constraint covering the columns `[code]` on the table `disease_category` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX `disease_category_code_key` ON `disease_category`(`code`);
