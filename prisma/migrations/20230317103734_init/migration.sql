-- CreateTable
CREATE TABLE `disease_category` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(191) NOT NULL,
    `code` VARCHAR(191) NOT NULL,
    `parent` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `age_range` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `rangeName` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `patient_type` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `patient` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `puskesmas` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `checkup` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `disease_categoryId` INTEGER NOT NULL,
    `age_rangeId` INTEGER NOT NULL,
    `patient_typeId` INTEGER NOT NULL,
    `gender` ENUM('L', 'P') NOT NULL,
    `amount` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `log_checkup` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `patientId` INTEGER NOT NULL,
    `puskesmasId` INTEGER NOT NULL,
    `checkupId` INTEGER NOT NULL,
    `date_checkup` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `checkup` ADD CONSTRAINT `checkup_disease_categoryId_fkey` FOREIGN KEY (`disease_categoryId`) REFERENCES `disease_category`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `checkup` ADD CONSTRAINT `checkup_age_rangeId_fkey` FOREIGN KEY (`age_rangeId`) REFERENCES `age_range`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `checkup` ADD CONSTRAINT `checkup_patient_typeId_fkey` FOREIGN KEY (`patient_typeId`) REFERENCES `patient_type`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `log_checkup` ADD CONSTRAINT `log_checkup_patientId_fkey` FOREIGN KEY (`patientId`) REFERENCES `patient`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `log_checkup` ADD CONSTRAINT `log_checkup_puskesmasId_fkey` FOREIGN KEY (`puskesmasId`) REFERENCES `puskesmas`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `log_checkup` ADD CONSTRAINT `log_checkup_checkupId_fkey` FOREIGN KEY (`checkupId`) REFERENCES `checkup`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
