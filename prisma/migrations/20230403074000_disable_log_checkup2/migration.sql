/*
  Warnings:

  - A unique constraint covering the columns `[disease_categoryId]` on the table `checkup` will be added. If there are existing duplicate values, this will fail.

*/
-- DropIndex
DROP INDEX `log_checkup_checkupId_fkey` ON `log_checkup`;

-- CreateIndex
CREATE UNIQUE INDEX `checkup_disease_categoryId_key` ON `checkup`(`disease_categoryId`);
