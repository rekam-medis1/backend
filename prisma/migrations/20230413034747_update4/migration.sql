/*
  Warnings:

  - You are about to drop the column `age_rangeId` on the `checkup` table. All the data in the column will be lost.
  - You are about to drop the column `disease_categoryId` on the `checkup` table. All the data in the column will be lost.
  - You are about to drop the column `patient_typeId` on the `checkup` table. All the data in the column will be lost.
  - Added the required column `age_range_id` to the `checkup` table without a default value. This is not possible if the table is not empty.
  - Added the required column `disease_category_id` to the `checkup` table without a default value. This is not possible if the table is not empty.
  - Added the required column `patient_type_id` to the `checkup` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE `checkup` DROP FOREIGN KEY `checkup_age_rangeId_fkey`;

-- DropForeignKey
ALTER TABLE `checkup` DROP FOREIGN KEY `checkup_disease_categoryId_fkey`;

-- DropForeignKey
ALTER TABLE `checkup` DROP FOREIGN KEY `checkup_patient_typeId_fkey`;

-- AlterTable
ALTER TABLE `checkup` DROP COLUMN `age_rangeId`,
    DROP COLUMN `disease_categoryId`,
    DROP COLUMN `patient_typeId`,
    ADD COLUMN `age_range_id` VARCHAR(191) NOT NULL,
    ADD COLUMN `disease_category_id` VARCHAR(191) NOT NULL,
    ADD COLUMN `patient_type_id` VARCHAR(191) NOT NULL;

-- AddForeignKey
ALTER TABLE `checkup` ADD CONSTRAINT `checkup_disease_category_id_fkey` FOREIGN KEY (`disease_category_id`) REFERENCES `disease_category`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `checkup` ADD CONSTRAINT `checkup_age_range_id_fkey` FOREIGN KEY (`age_range_id`) REFERENCES `age_range`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `checkup` ADD CONSTRAINT `checkup_patient_type_id_fkey` FOREIGN KEY (`patient_type_id`) REFERENCES `patient_type`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
