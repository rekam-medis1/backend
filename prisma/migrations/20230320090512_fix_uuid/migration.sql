/*
  Warnings:

  - The primary key for the `age_range` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `disease_category` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `patient` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `patient_type` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `puskesmas` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `user` table will be changed. If it partially fails, the table could be left without primary key constraint.

*/
-- DropForeignKey
ALTER TABLE `checkup` DROP FOREIGN KEY `checkup_age_rangeId_fkey`;

-- DropForeignKey
ALTER TABLE `checkup` DROP FOREIGN KEY `checkup_disease_categoryId_fkey`;

-- DropForeignKey
ALTER TABLE `checkup` DROP FOREIGN KEY `checkup_patient_typeId_fkey`;

-- DropForeignKey
ALTER TABLE `log_checkup` DROP FOREIGN KEY `log_checkup_patientId_fkey`;

-- DropForeignKey
ALTER TABLE `log_checkup` DROP FOREIGN KEY `log_checkup_puskesmasId_fkey`;

-- AlterTable
ALTER TABLE `age_range` DROP PRIMARY KEY,
    MODIFY `id` VARCHAR(191) NOT NULL,
    ADD PRIMARY KEY (`id`);

-- AlterTable
ALTER TABLE `checkup` MODIFY `disease_categoryId` VARCHAR(191) NOT NULL,
    MODIFY `age_rangeId` VARCHAR(191) NOT NULL,
    MODIFY `patient_typeId` VARCHAR(191) NOT NULL;

-- AlterTable
ALTER TABLE `disease_category` DROP PRIMARY KEY,
    MODIFY `id` VARCHAR(191) NOT NULL,
    ADD PRIMARY KEY (`id`);

-- AlterTable
ALTER TABLE `log_checkup` MODIFY `patientId` VARCHAR(191) NOT NULL,
    MODIFY `puskesmasId` VARCHAR(191) NOT NULL;

-- AlterTable
ALTER TABLE `patient` DROP PRIMARY KEY,
    MODIFY `id` VARCHAR(191) NOT NULL,
    ADD PRIMARY KEY (`id`);

-- AlterTable
ALTER TABLE `patient_type` DROP PRIMARY KEY,
    MODIFY `id` VARCHAR(191) NOT NULL,
    ADD PRIMARY KEY (`id`);

-- AlterTable
ALTER TABLE `puskesmas` DROP PRIMARY KEY,
    MODIFY `id` VARCHAR(191) NOT NULL,
    ADD PRIMARY KEY (`id`);

-- AlterTable
ALTER TABLE `user` DROP PRIMARY KEY,
    MODIFY `id` VARCHAR(191) NOT NULL,
    ADD PRIMARY KEY (`id`);

-- AddForeignKey
ALTER TABLE `checkup` ADD CONSTRAINT `checkup_disease_categoryId_fkey` FOREIGN KEY (`disease_categoryId`) REFERENCES `disease_category`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `checkup` ADD CONSTRAINT `checkup_age_rangeId_fkey` FOREIGN KEY (`age_rangeId`) REFERENCES `age_range`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `checkup` ADD CONSTRAINT `checkup_patient_typeId_fkey` FOREIGN KEY (`patient_typeId`) REFERENCES `patient_type`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `log_checkup` ADD CONSTRAINT `log_checkup_patientId_fkey` FOREIGN KEY (`patientId`) REFERENCES `patient`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `log_checkup` ADD CONSTRAINT `log_checkup_puskesmasId_fkey` FOREIGN KEY (`puskesmasId`) REFERENCES `puskesmas`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
