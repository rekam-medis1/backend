-- Adminer 4.8.1 MySQL 8.0.32-0ubuntu0.22.04.2 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

CREATE TABLE `disease_category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `disease_category_code_key` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `disease_category` (`id`, `name`, `code`, `parent`) VALUES
(1,	'PENYAKIT INFEKSI PADA USUS',	'01',	NULL),
(2,	'Cholera',	'A00',	1),
(3,	'Typhoid Fever',	'A01',	1),
(4,	'Syghelosis',	'A03',	1),
(6,	'Amoebiasis',	'A06',	1),
(7,	'Diare',	'A09',	1),
(8,	'PENYAKIT TUBERKULOSA',	'02',	NULL),
(9,	'\'TB Paru BTA (+)',	'A15',	8),
(10,	'\'TB Klinis  Termasuk Rongent (+) BTA (-)  ',	'A16',	8),
(11,	'TB Organ Lain',	'A18',	8),
(12,	'PENYAKIT BAKTERI',	'03',	NULL),
(13,	'Lepra',	'A30',	12),
(14,	'Tetanus Lainnya',	'A35',	12),
(16,	'Diphtheria',	'A36',	12),
(17,	'Batuk Rejan',	'A37',	12),
(18,	'Acute Poliomyelitis',	'A80',	12);

-- 2023-04-17 01:41:44